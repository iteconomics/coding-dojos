package de.iteconomics.dojo.primenumber;

import static org.junit.Assert.assertThat;

import java.util.ArrayList;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

public class GeneratePrimTest {

	@Test
	public void generateWith_1_result_emptyList() {
		ArrayList<Integer> result = new ArrayList<>();
		assertThat(GeneratePrim.generate(1),CoreMatchers.is(result));
	}
	
	@Test
	public void generateWith_2_result_2() {
		assertThat(GeneratePrim.generate(2),CoreMatchers.is(createList(2)));
	}
	
	@Test
	public void generateWith_4_result_2_2() {
		assertThat(GeneratePrim.generate(4),CoreMatchers.is(createList(2,2)));
	}
	
	@Test
	public void generateWith_3_result_3() {
		assertThat(GeneratePrim.generate(3),CoreMatchers.is(createList(3)));
	}
	
	@Test
	public void generateWith_5_result_5() {
		assertThat(GeneratePrim.generate(5),CoreMatchers.is(createList(5)));
	}
	
	@Test
	public void generateWith_60_result_2_2_3_5() {
		assertThat(GeneratePrim.generate(60),CoreMatchers.is(createList(2,2,3,5)));
	}
	
	@Test
	public void generateWith_59_result_59() {
		assertThat(GeneratePrim.generate(59),CoreMatchers.is(createList(59)));
	}
	
	private static ArrayList<Integer> createList(int... numbers)
	{
		ArrayList<Integer> list = new ArrayList<>();
		for (int number : numbers)
		{
			list.add(number);
		}
		return list;
	}

}
