package de.iteconomics.dojo.primenumber;

import java.util.ArrayList;

public class GeneratePrim {

	public static ArrayList<Integer> generate(int numberToFactor) {
		ArrayList<Integer> primes = new ArrayList<>();
		for (int candidate = 2; candidate<=numberToFactor; candidate++)
		{
			while (numberToFactor % candidate == 0 ) {
				primes.add(candidate);
				numberToFactor /= candidate;
			}
		}
		return primes;
	}

}
