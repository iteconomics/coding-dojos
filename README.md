# README #

Das hier ist das Repository für den Code, der in den Coing Dojos der Softwarekammer München bei den Session der it-economics erzeugt und geteilt werden soll.


### How do I get set up? ###

  There ist no setup.
  Normalerweise wird der Code in Eclipse oder IntelliJ erzeugt, manchmal ist es aber noch nicht mal Java, was wir hier teilen.
  Hier stehen also nur die Dateien mit dem wichtigsten Code. Project Setup und die Beschaffung der Abhängigkeiten sollten aber 
  jeweils nicht allzu kompliziert sein. Bei Fragen könnt ihr euch gerne an mich (mgrosholz@it-economics.de) wenden.
